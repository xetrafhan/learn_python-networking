#!/usr/bin/env python3
# Foundations of Python Network Programming, Third Edition
# https://github.com/brandon-rhodes/fopnp/blob/m/py3/chapter01/search3.py

from __future__ import unicode_literals
import http.client
import json
from urllib.parse import quote_plus


def geocode(address):
    base = '/maps/api/geocode/json'
    # '/maps/api/geocode/json?address=207+N.+Defiance+St%2C+Archbold%2C+OH&sensor=false'
    path = '{}?address={}&sensor=false'.format(base, quote_plus(address))
    connection = http.client.HTTPConnection('maps.google.com')
    connection.request('GET', path)

    rawreply = connection.getresponse().read()
    reply = json.loads(rawreply.decode('utf-8'))

    print(reply['results'][0]['geometry']['location'])

if __name__ == '__main__':
    geocode('207 N. Defiance St, Archbold, OH')
